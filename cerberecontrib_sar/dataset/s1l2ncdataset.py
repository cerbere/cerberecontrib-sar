# -*- coding: utf-8 -*-
"""
Mapper class for Sentinel-1 L2 files

:copyright: Copyright 2018 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import numpy as np
import xarray as xr

from cerbere.dataset.dataset import Dataset


class S1L2NCDataset(Dataset):
    """Abstract mapper class for Sentinel-1 L2 files
    """
    def __init__(self, *args, prefix, **kwargs):
        super(S1L2NCDataset, self).__init__(
            *args,
            dim_matching={
                'time': 'time',
                f'{prefix}AzSize': 'row',
                f'{prefix}RaSize': 'cell'
            },
            field_matching={
                'time': 'time',
                f'{prefix}Lat': 'lat',
                f'{prefix}Lon': 'lon'
            },
            prefix=prefix,
            **kwargs
        )

    def _open_dataset(self, prefix, **kwargs):
        dst = super(S1L2NCDataset, self)._open_dataset(**kwargs)

        # rename attributes
        dst.attrs['time_coverage_start'] = dst.attrs.pop('firstMeasurementTime')
        dst.attrs['time_coverage_end'] = dst.attrs.pop('lastMeasurementTime')

        # fix some types
        dst.attrs['statevectorPos'] = dst.attrs['statevectorPos'].tolist()
        dst.attrs['statevectorVel'] = dst.attrs['statevectorVel'].tolist()
        dst.attrs['statevectorAcc'] = dst.attrs['statevectorAcc'].tolist()

        # add time variable (take mean time)
        dates = np.array([
            np.datetime64(dst.attrs['time_coverage_start']),
            np.datetime64(dst.attrs['time_coverage_end'])
        ])
        delta_sec = np.timedelta64(1, 's')
        epoch = '1970-01-01T00:00:00'
        epoch_sec = (dates - np.datetime64(epoch)) / delta_sec
        epoch_sec_mean = np.mean(epoch_sec)
        dt_mean = np.datetime64(epoch) + \
                  np.timedelta64(int(epoch_sec_mean), 's')

        dst['time'] = xr.DataArray(
            data=[dt_mean],
            dims=('obs',),
            attrs={
                'long_name': 'time',
                'standard_name': 'time'
            }
        )
        dst = dst.rename({
            f'{prefix}Lat': 'lat',
            f'{prefix}Lon': 'lon',
        })
        dst = dst.set_coords(['time', 'lat', 'lon'])

        # keep only relevant variables
        dst = dst.drop_vars(
            [_ for _ in dst.data_vars if not _.startswith(prefix)])

        return dst

        
class S1L2OWINCDataset(S1L2NCDataset):
    """Abstract mapper class for Sentinel-1 L2 OWI products
    """
    def __init__(self, *args, **kwargs):
        super(S1L2OWINCDataset, self).__init__(
            *args,
            prefix="owi",
            **kwargs
        )


class S1L2OSWNCDataset(S1L2NCDataset):
    """Abstract mapper class for Sentinel-1 L2 OSW products
    """
    def __init__(self, *args, **kwargs):
        super(S1L2OSWNCDataset, self).__init__(
            *args,
            prefix="osw",
            **kwargs
        )


class S1L2RVLNCDataset(S1L2NCDataset):
    """Abstract mapper class for Sentinel-1 L2 RVL products
    """
    def __init__(self, *args, **kwargs):
        super(S1L2RVLNCDataset, self).__init__(
            *args,
            prefix="rvl",
            **kwargs
        )
