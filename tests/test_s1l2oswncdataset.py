"""

Test class for ESA Sentinel-1 L2 OSW files

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import unittest

from tests.checker import Checker


class S1L2OSWNCDatasetChecker(Checker, unittest.TestCase):
    """Test class for ESA Sentinel-1 L2 OSW files"""

    def __init__(self, methodName="runTest"):
        super(S1L2OSWNCDatasetChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the mapper class name"""
        return 'S1L2OSWNCDataset'

    @classmethod
    def feature(cls):
        """Return the related feature class name"""
        return 'Point'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "S1A_WV_OCN__2SSV_20200711T101845_20200711T102622_033405_03DEE1_BC71.SAFE/measurement/s1a-wv1-ocn-vv-20200711t101845-20200711t101848-033405-03dee1-001.nc"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"
