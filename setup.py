# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

long_desc = '''
This package contains the Cerbere extension for various SAR file 
products and formats.
'''

requires = ['cerbere>=2.0']

setup(
    name='cerberecontrib-sar',
    version='1.0',
    url='',
    download_url='',
    license='GPLv3',
    author='Jeff Piolle',
    author_email='jfpiolle@ifremer.fr',
    description='Cerbere extension for various SAR file products and formats',
    long_description=long_desc,
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GPLv3 License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Documentation',
        'Topic :: Utilities',
    ],
    platforms='any',
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        'cerbere.plugins': [
            'S1L2OWINCDataset = '
            'cerberecontrib_sar.dataset.s1l2ncdataset:S1L2OWINCDataset',
            'S1L2OSWNCDataset = '
            'cerberecontrib_sar.dataset.s1l2ncdataset:S1L2OSWNCDataset',
            'S1L2RVLNCDataset = '
            'cerberecontrib_sar.dataset.s1l2ncdataset:S1L2RVLNCDataset',
        ]
    }
)

